sed -i 's:@string/GOOGLE_MAPS_API_KEY:${GOOGLE_MAPS_API_KEY}:g' android/app/src/main/res/values/strings.xml
sed -i 's:Config.GOOGLE_MAPS_API_KEY:"${GOOGLE_MAPS_API_KEY}":g' src/components/map.js

envsub android/app/src/main/res/values/strings.xml --system
envsub src/components/map.js --system

echo 'Your keys have been added to the project.'
